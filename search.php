<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" media="screen" href="styles.css">
    <title>Search Results</title>
</head>
<body>
    <form action="home.php">
        <input type="submit" value="Restart Search">
    </form>
    <br>
    <!-- Filter by Date
    <label for = 'startDate'>Enter Start Date</label>
    <input type='date' id='startDate' placeholder = 'yyyy-mm-dd'/>
    <label for = 'endDate'>Enter End Date</label>
    <input type='date' id='endDate' placeholder = 'yyyy-mm-dd'/> -->

    <?php
    session_start();
    if (isset($_POST['keywords']) && !empty($_POST['keywords'])) {
        ?>
        <strong> Search results for: </strong><?php echo htmlentities($_POST['keywords']);?>
        <?php
        // echo "success: ".$_POST['keywords'];
        $search_html = filter_input(INPUT_POST, 'keywords', FILTER_SANITIZE_SPECIAL_CHARS);
        $search_url = trim(filter_input(INPUT_POST, 'keywords', FILTER_SANITIZE_ENCODED));
        $_SESSION['keywords'] = $search_html;
        $_SESSION['filterLocation'] = '';
        if (isset($_POST['filterLocation']) && !empty($_POST['filterLocation'])) {
            $_SESSION['filterLocation'] = trim(filter_input(INPUT_POST, 'filterLocation', FILTER_SANITIZE_SPECIAL_CHARS));
            ?>
            <br><strong> Location filtered by: </strong><?php echo htmlentities($_SESSION['filterLocation']);?>
            <?php
        }
        ?>
        <form action="search.php" method = "POST"> 
        <p>
            <label for = 'filterLocation'>Enter Location To Filter For: </label>
            <input type='text' name ='filterLocation'id='filterLocation' placeholder = 'e.g. AFRC'/>
            <input type='hidden' name = 'keywords' value = "<?php echo $_POST['keywords'];?>"/>
            <input type="submit" value="Filter!" />
        </p>
        </form>
        
        <?php

        $json_file = file_get_contents("https://images-api.nasa.gov/search?q=$search_url");
        $jfo = json_decode($json_file);
        $items = $jfo->collection->items;
        //$total_hits = $jfo->collection->metadata->total_hits;
        $img_urls = array();
        foreach ($items as $i) {
            $locationFound = false;
            $file = $i->data;
            foreach ($file as $f) {
                foreach ($f as $key => $value) {
                    if ($key == 'location') {
                        $locationFound = true;
                        break;
                    }
                }
                // $data_array = array(

                // )
                // $location = $f->location;
                if ($locationFound) {
                    $location = trim($f->location);
                }
                $desc = $f->description;
                $file_type = $f->media_type;
                $file_title = $f->title;
                $nasa_id = $f->nasa_id;
                $date_created = substr($f->date_created, 0, 10);

                //echo $desc;
            }
            if (!empty($_SESSION['filterLocation'])) {
                if ($locationFound && $location == $_SESSION['filterLocation']) {
                    $links = $i->links;
                    foreach ($links as $l) {
                        //make sure we only display images and not captions
                        if (($l->rel) == "preview") {
                            $image_url = $l->href;
                            array_push($img_urls, array($image_url, $file_type, $desc, $file_title, $nasa_id, $date_created, true, $location));
                            // <img src = "<?php echo $image_url;">
                            
                        }
                    }
                }
            }
            else {
                $links = $i->links;
                foreach ($links as $l) {
                    //make sure we only display images and not captions
                    if (($l->rel) == "preview") {
                        $image_url = $l->href;
                        if ($locationFound) {
                            array_push($img_urls, array($image_url, $file_type, $desc, $file_title, $nasa_id, $date_created, true, $location));
                        }
                        else {
                            array_push($img_urls, array($image_url, $file_type, $desc, $file_title, $nasa_id, $date_created, false, ''));

                        }
                        // <img src = "<?php echo $image_url;">
                    }
                }
            }
        }
        $img_urls_size = count($img_urls);
        ?>
        <div class = "row">
            <div class = "column">
                <?php
                for ($i=0; $i<ceil($img_urls_size/3); $i++) {
                    ?>
                    <img src = "<?php echo $img_urls[$i][0];?>" class="thumb">
                    <!-- reference: https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_image_grid -->
                    <?php
                    echo "<strong> File Number: </strong>".$i." ";
                    echo "<strong> File Type: </strong>".$img_urls[$i][1]." ";
                    echo "<strong> File Title: </strong>".$img_urls[$i][3]." ";
                    echo "<strong> Date Created: </strong>".$img_urls[$i][5]." ";
                    echo "<strong> NASA ID: </strong>".$img_urls[$i][4]." ";
                    if (count($img_urls[$i])>7 && $img_urls[$i][6]) {
                        echo "<strong> Location: </strong>".$img_urls[$i][7]." ";
                    }
                    echo "<br><br>";
                }
                    ?>
            </div>
            <div class = "column">
                <?php
                for ($i=ceil($img_urls_size/3); $i<ceil(2*$img_urls_size/3); $i++) {
                    ?>
                    <img src = "<?php echo $img_urls[$i][0];?>" class = "thumb"> 
                    <?php
                    echo "<strong> File Number: </strong>".$i." ";
                    echo "<strong> File Type: </strong>".$img_urls[$i][1]." ";
                    echo "<strong> File Title: </strong>".$img_urls[$i][3]." ";
                    echo "<strong> Date Created: </strong>".$img_urls[$i][5]." ";
                    echo "<strong> NASA ID: </strong>".$img_urls[$i][4]." ";
                    if (count($img_urls[$i])>7 && $img_urls[$i][6]) {
                        echo "<strong> Location: </strong>".$img_urls[$i][7]." ";
                    }
                    echo "<br><br>";
                }
                ?>
            </div>
            <div class = "column">
                <?php
                for ($i=ceil(2*$img_urls_size/3); $i<$img_urls_size; $i++) {
                    ?>
                    <img src = "<?php echo $img_urls[$i][0];?>" class = "thumb">
                    <?php
                    echo "<strong> File Number: </strong>".$i." ";
                    echo "<strong> File Type: </strong>".$img_urls[$i][1]." ";
                    echo "<strong> File Title: </strong>".$img_urls[$i][3]." ";
                    echo "<strong> Date Created: </strong>".$img_urls[$i][5]." ";
                    echo "<strong> NASA ID: </strong>".$img_urls[$i][4]." ";
                    if (count($img_urls[$i])>7 && $img_urls[$i][6]) {
                        echo "<strong> Location: </strong>".$img_urls[$i][7]." ";
                    }
                    echo "<br><br>";
                }
                ?>
            </div>

        </div>
        <?php
        //print_r($img_urls);
    }
    else if (isset($_POST['nasa_id']) && !empty($_POST['nasa_id'])) {
        ?>
        <strong> Search results for: </strong><?php echo htmlentities($_POST['nasa_id']);?>
        <br>
        <?php
        $search_html = filter_input(INPUT_POST, 'nasa_id', FILTER_SANITIZE_SPECIAL_CHARS);
        $search_url = trim(filter_input(INPUT_POST, 'nasa_id', FILTER_SANITIZE_ENCODED));
        $json_file = file_get_contents("https://images-api.nasa.gov/asset/$search_url");
        if ($json_file != false) {
        
            $jfo = json_decode($json_file);
            $items = $jfo->collection->items;
            $preview_success = false;
            foreach ($items as $i) {
                $i_display = $i->href;
                //check if the image was in jpg format, avoids trying to display image formats that the browser does not support
                if (substr($i_display, strlen($i_display)-4,strlen($i_display))==".jpg") {
                    echo "File Preview: <br>";
                    ?>
                    <img src = "<?php echo $i_display;?>" class="id_preview">
                    <?php
                    $preview_success = true;
                    break;
                }
            }
            echo "<br><br><strong>Below are the links associated with this NASA ID: </strong>";
            foreach ($items as $i) {
                if (!$preview_success) {
                    echo "<br>Sorry. No preview could be generated at this time. <br>";
                }
                $i_link = $i->href;
                //print out all links found
                printf("\t<li>%s</li>\n",
                "Link: $i_link"
                );
            }
        }
        else {
            echo "<strong> ERROR: Item Not Found. Please Try Again. </strong>";
        }

    }
    else {
        echo "<strong>Please Try Again. </strong>";
    }




    ?>
    <!-- <img src ="https://images-assets.nasa.gov/image/ED07-0204-46/ED07-0204-46~thumb.jpg"> -->
    
    <form action="home.php">
        <input type="submit" value="Restart Search">
    </form>
</body>
</html>